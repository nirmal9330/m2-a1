import axios, { Axios } from "axios";


const baseURL="https://www.googleapis.com/books/v1/volumes?";

function get(url)
{
    console.log(baseURL+url);
    return axios.get(baseURL+url);
}

export default{
    get,
}