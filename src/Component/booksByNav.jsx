import React,{Component} from "react";
import http from "../Servecies/serverBook";
import LeftPannel from "./leftPannelBooks";
import BooksNav from "./booksDisplay";
import {Route,Redirect,Switch} from "react-router-dom";
import queryString from "query-string";
class BooksHome extends Component
{
    state={
        BooksArr:[],
        option:require("./settingOption.js"),
        MyBookCart:require("./cart1.js"),
        qValue: "",
    };


    async componentDidMount()
    {
        let queryParams=queryString.parse(this.props.location.search);
        let searchString=this.makesearchString(queryParams);
        
        this.fetchData();
    }

     
    async componentDidUpdate(prevProps,prevState)

    {
           if(prevState.option.option!==this.state.option.option)
           {
               this.fetchData();
           }
    }
    
    async fetchData()
    {
        console.log(this.state.option.option);
        console.log("After Update State");
        let queryParams=queryString.parse(this.props.location.search);
        let searchString=this.makesearchString(queryParams);
        console.log("searchString",searchString);
       
        
        let response =await http.get(`&${searchString}`);
        let {data}=response;
       
        this.setState({BooksArr:data.items});
       
     
        //window.location=`/books/${searchString}`;
         
    }


     
    makesearchString(queryParams)
    {
      let {option,qValue}=this.state;
      let {value}=this.props.match.params;
      let q=value;
       console.log("opt2",option.option.startIndex);
       let {startIndex=option.option.startIndex,maxResults=option.option.maxSize ? option.option.maxSize : 8}=queryParams;
    let {printType="", langRestrict="",filter="",orderBy=""}=queryParams;
   
      let searchString="";
      searchString=this.addToQueryString(searchString,"startIndex",startIndex);
      searchString=this.addToQueryString(searchString,"maxResults",maxResults);
      searchString=this.addToQueryString(searchString,"q",q);
      searchString=this.addToQueryString(searchString,"printType",printType);
      searchString=this.addToQueryString(searchString,"langRestrict",langRestrict);
      searchString=this.addToQueryString(searchString,"filter",filter);
      searchString=this.addToQueryString(searchString,"orderBy",orderBy);
    
      return searchString;
    }
    
    
    addToQueryString=(str,name,value)=>
    value ? str ? `${str}&${name}=${value}`:
     `${name}=${value}` : str ;
    
    
     handlePage=(index)=>
     {
         let {option}=require("./settingOption.js");
         console.log("opt",option);
        let queryParams=queryString.parse(this.props.location.search);
        queryParams.startIndex=(+queryParams.startIndex)+(index*(+queryParams.maxResults));
        option.startIndex=option.startIndex+(index*(option.maxSize));
       this.callURL(`/books/${queryParams.q}`,queryParams);
     }
    
     addToMyBooksHandler=(obj,value)=>
     {
        let {MyBookCart}=require("./cart1.js");
        let s1={...this.state};
        if(value==="Add")
        {
          
            MyBookCart.push(obj);
        }
        else if(value==="Remove")
        {
            let index1=MyBookCart.findIndex((el)=>el.id===obj.id);
           
            MyBookCart.splice(index1,1);
        }
      this.setState(s1);
     }


     onOptionChangeHandler=(option)=>
     {
        let queryParams=queryString.parse(this.props.location.search);
         this.callURL(`/books/${queryParams.q}`,option);
         
        
     }
     callURL=(url,option)=>
     {

     let searchString=this.makesearchString(option);
     this.props.history.push({
       pathname:url,
       search:searchString,
   });
}
     
    render()
    {
        let {value}=this.props.match.params;
        let {BooksArr,MyBookCart}=this.state;
        let queryParams=queryString.parse(this.props.location.search);
      
        return(<React.Fragment>
            <div className="container">
             <div className="row">
                 <div className="col-3">
                 <LeftPannel  optionx={queryParams} onOptionChange={this.onOptionChangeHandler}/>
              </div>
                <div className="col-9">
                 <BooksNav  handlePage={this.handlePage} handleMyBooks={this.addToMyBooksHandler} option={queryParams}
                 Books={BooksArr}/>
               
                  </div>
                 </div>
           </div>
               
        </React.Fragment>);
    }
}
export default BooksHome;