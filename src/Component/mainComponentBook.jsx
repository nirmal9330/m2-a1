import React,{Component} from "react";
import Nav from "./navbarBooks";
import SettingsBook from "./settingBooks";
import MyBooks from "./myBooks";
import Books from "./booksM1";
import BooksBySearch from "./booksM2";
import {Route ,Redirect ,Switch} from "react-router-dom";
class BooksMain extends Component
{

    state={
     NavItems:"",
    };


    navChangeHandler=(str)=>
    {
     this.setState({NavItems:str});
    }
    render()
    {

     /*
 <BooksNav   Books={BooksArr} handlePage={this.handlePage} handleMyBooks={this.addToMyBooksHandler} 
                  option={queryParams}/>  
               <Route path="/books" component={BooksHome} Nvalue={NavItems}/>

           <Redirect from="/" to="/books"/>

           
           <Route path="/settings"  component={SettingsBook}/>
          <Route path="/myshelf"  component={MyBooks}/>
          
     */

       let {NavItems}=this.state;
    return(<React.Fragment>
        <Nav onNavChange={this.navChangeHandler}/>
        <Switch>
         
           <Route path='/books' render={(props)=><Books  {...props} value={NavItems}/>}/>
           <Route path="/settings"  component={SettingsBook}/>
           <Route path="/myshelf"  component={MyBooks}/>
           <Route path="/" component={BooksBySearch}/>
          
       </Switch>
    </React.Fragment>);
    }
}
export default BooksMain;