import React,{Component} from "react";
import {Link} from "react-router-dom";
class Nav extends Component
{

    state={
        Books:["Harry Potter","Agatha Christie","PremChand","Jane Austin"],
    };



    render()
    {
        let {Books}=this.state;
        return(<React.Fragment>
       <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <Link className="navbar-brand" to="/"><i className="fas fa-book-open"
         style={{width:'30%', height:'90%',marginLeft:'40px'}}></i></Link>
        <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto"  style={{hover:'text-success'}}>
               {Books.map((ele,index)=>(
                    <li className="nav-item"  key={index}  onClick={()=>this.props.onNavChange(ele)} >
                    <Link className="nav-link" to={`/books`}>{ele}
                   </Link>
                    </li>
                ))}
                  <li className="nav-item">
                    <a className="nav-link" href="#">
                  <Link to="/myshelf"  className="text-dark " style={{textDecoration:'none' }}>MyBooks</Link>
                    </a>
                    </li>
                    <li className="nav-item">
                    <a className="nav-link " href="#">
                  <Link to="/settings" className="text-dark" style={{textDecoration:'none' }}>Setting</Link>
                  </a>
                    </li>
                   
                </ul>
            </div>
        </nav>
    </React.Fragment>);
    }
}
export default Nav;