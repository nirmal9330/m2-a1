import React,{Component} from "react";
class SettingsBook extends Component
{


    
handleChange=(e)=>
{
   let {currentTarget : input}=e;
   let s1={...this.state};
   if(input.type==="text")
   {
    s1.settingOption[input.name]=input.value;
   }
   else{
   s1.settingOption[input.name]=input.checked;
   }
  // console.log(input.name+"-->"+input.checked);
   this.setState(s1);
}


async componentDidMount()
{
    console.log("Mounted");
    let {option}=require("./settingOption.js");
   let s1={...this.state};
   let {settingOption}=this.state;
   s1.settingOption["printType"]=option["printType"];
   settingOption["language"]=option["language"];
   settingOption.filter=option["filter"];
   settingOption.orderBy=option["orderBy"];
   settingOption.maxSize=option["maxSize"];
   this.setState(s1);
   
  
}

async componentDidUpdate(prevProps,prevState)
{
    if(prevState!==this.state)
   {

    let {option}=require("./settingOption.js");

    let {settingOption}=this.state;
    option["printType"]=settingOption.printType ; 
    option["language"]=settingOption.language  ;
    option["filter"]=settingOption.filter ;
    option["orderBy"]=settingOption.orderBy ; 
    option["maxSize"]=settingOption.maxSize ;

   
  

   }
}


    state={
        option:require("./settingOption.js"),
        Arr:["PrintType --(Restrict To Books Or Magazines)","languages--(Restricts the volumes returned to those that are tagged with the specified languages)",
        "Filter--(Filter Search results By Volume type and and availaibity)","orderBy--(order of the volume search result)"],
      settingOption:{
          printType:Boolean,
          language:Boolean,
          filter:Boolean,
          orderBy:Boolean,
          maxSize:Number,
    
    },
};




    
showCheckBoxes=(label,name,Selvalue)=>
{
    return (<React.Fragment>
      <div className="form-check">
       <input type="checkbox" className="form-check-input" name={name} 
       onChange={this.handleChange}  value={Selvalue} checked={Selvalue}/>
       <label className="form-check-label">{label}</label>
         </div>  
    </React.Fragment>
    );
}
showTextField=(label,name,value)=>
{
  return(<React.Fragment>
      <div className="form-group">
          <label className="form-control-label text-success">{label}</label>
          <input className="form-control" type="text" name={name} value={value} onChange={this.handleChange}/>
          </div>
  </React.Fragment>);
}


/*
"PrintType --(Restrict To Books Or Magazines)"
*/

    render()
    { let {Arr,settingOption}=this.state;
    let {printType,language,filter,orderBy,maxSize}=settingOption;
    let {option}=require("./settingOption.js");
    console.log(option);
    
        return(<React.Fragment>
          <div className="container col-6 mx-auto">
           <h3 className="text-danger">Select Option For Filtering On Left Panel</h3><br/>
        {this.showCheckBoxes(Arr[0],"printType",settingOption.printType)}
        {this.showCheckBoxes(Arr[1],"language",settingOption.language)}
        {this.showCheckBoxes(Arr[2],"filter",settingOption.filter)}
        {this.showCheckBoxes(Arr[3],"orderBy",settingOption.orderBy)}
         {this.showTextField(Arr[4],"maxSize",settingOption.maxSize)}
         </div>
        </React.Fragment>);
    }
}
export default SettingsBook;