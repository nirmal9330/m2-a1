import React,{Component} from "react";

class MyBooks extends Component
{
    state={};

    removeBooksHandler=(id)=>
    {
        let s1={...this.state};
        let {MyBookCart}=require("./cart1.js");
        let index=MyBookCart.findIndex((el)=>el.id===id);
        MyBookCart.splice(index,1);
        this.setState(s1);
    }


    render()
    {
        let {MyBookCart}=require("./cart1.js");
     
         return(<React.Fragment>
 <div className="container">
           {MyBookCart.length===0  ? 
           <h4 className="text-center bg-info">No Books In My Books</h4>   
          : 
       <div className="row text-center">
                   {MyBookCart.map((ele,index)=>(
                       <div className={MyBookCart.length<=2 ? "col-6 bg-success text-center border" :MyBookCart.length===3 ? "col-4 bg-success text-center border" : "col-3 bg-success text-center border" } key={index}>
                <React.Fragment>
                  
                    <div className="text-center">
                            <img src={ele.volumeInfo.imageLinks?ele.volumeInfo.imageLinks.thumbnail
                            :""}></img>
                            </div>

                            <div className="text-center">
                            <h4>{ele.volumeInfo.title}</h4>
                            </div>

                            <div className="text-center">
                            {ele.volumeInfo.authors?ele.volumeInfo.authors.map((el,index)=>(<p key={index}>{el}</p>)): ""}
                            </div>
                            <div className="text-center">
                            {  ele.volumeInfo.categories ? ele.volumeInfo.categories.map((el,index)=>(<p key={index}>{el}</p>)) : ""}
                            </div>
                           

                            <div className="text-center">
                           <button className="btn btn-secondary" 
        onClick={()=>this.removeBooksHandler(ele.id)}>Remove from MyBooks</button>
                            </div>
                           
                    
                   
                  </React.Fragment>
                  </div>
                 
                   ))}
                 
             </div>
    }
           </div>


         </React.Fragment>);
    }
}
export default MyBooks;