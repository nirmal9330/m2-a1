import React,{Component} from "react"
import queryString from "query-string";
import LeftPannel from "./leftPannelBooks";
import BooksNav from "./booksDisplay";
import http from "../Servecies/serverBook";
import {Route,Redirect,Switch} from "react-router-dom";
class BooksBySearch extends Component
{
    state={
        searchText:"",
        Books:[],
        showBooks:false,
        option:require("./settingOption.js"),
    };

    async componentDidMount()
    {
       this.fetchData();
    }

    
async fetchData()
{
 
   let {value}=this.props;
   let {searchText}=this.state;
    let queryParams=queryString.parse(this.props.location.search);
    let searchString=this.makeSerchstring(queryParams);

    let response= await http.get(`&${searchString}`);
    let {data}=response;
   this.setState({Books:data.items,showBooks:true});
}
  handleChange=(e)=>
    {
      let {currentTarget : input}=e;
    let s1={...this.state};
      s1[input.name]=input.value;
     this.setState(s1);
    }

callURL=(url,option)=>
{let searchString=this.makeSerchstring(option);
  this.props.history.push({
       pathname:url,
       search:searchString,
   });
}
makeSerchstring(queryParams)
{
    let {value=""}=this.props.match.params;
   let {option}=this.state;
    console.log("option",option);
   
    let {startIndex="0",maxResults=option.option.maxSize ?option.option.maxSize: "8",q=value ? value : this.state.searchText}=queryParams;
   let {langRestrict,filter,printType,orderBy}=queryParams;
    let searchString="";
  
  searchString=this.addToQueryString(searchString,"startIndex",startIndex);
  searchString=this.addToQueryString(searchString,"maxResults",maxResults);
  searchString=this.addToQueryString(searchString,"q",q);
  searchString=this.addToQueryString(searchString,"langRestrict",langRestrict);
  searchString=this.addToQueryString(searchString,"filter",filter);
  searchString=this.addToQueryString(searchString,"printType",printType);
  searchString=this.addToQueryString(searchString,"orderBy",orderBy);
  return searchString;

}

addToQueryString=(str,name,value)=>
value ? str ? `${str}&${name}=${value}`:
 `${name}=${value}` : str ;




    handleSearch=(e)=>
    {
       e.preventDefault();
       let queryParams=queryString.parse(this.props.location.search);
       this.setState({showBooks:true});
       this.callURL("/books",queryParams);
     
    }

   showSearchFields=()=>
   {
       let {searchText}=this.state;
       return(<React.Fragment>
           <div className="conatiner">
              <div className="col-8 mx-auto bg-light text-center">
                  <div className="col-12 " >
                      <br/>
                      <img  style={{width:'50%',height:'60%'}} className="img-circle rounded-circle"
                      src="https://news.yale.edu/sites/default/files/styles/featured_media/public/adobestock_185877428.jpeg?itok=9Zq9DnoZ&c=40499141215ed501bb0ad8e2462cdde3">
                    </img>
                    <br/><br/>
                      </div>
                      <div className="row">
                      <div className="col-6 ">
                          <div className="form-group">
                              <input className="form-control" type="text" name="searchText" value={searchText}
                              onChange={this.handleChange} style={{marginLeft:'50%'}}/>
                              </div>
                              </div>
                              <div className="col-2"></div>
                              <div className="col-3">
                                  <button className="btn btn-primary"  
                                  onClick={this.handleSearch}>Search</button>
                                  </div>
                              </div>
                          
                           </div>
                            </div>

       </React.Fragment>);
   }

   handlePageChange=(index)=>
   {
     let s1={...this.state};
       let queryParams=queryString.parse(this.props.location.search);
       queryParams.startIndex=(+queryParams.startIndex)+(index*(+queryParams.maxResults));
      this.callURL("/books",queryParams);
   }

   onOptionChangeHandler=(option)=>
   {
   
     this.callURL("/books",option);
   } 

    render()
    {
        let {search=""}=this.props;
        let queryParams=queryString.parse(this.props.location.search);
         let {showBySearch}=this.props;
        let {Books,showBooks,searchText}=this.state;
      return(<React.Fragment>
          
          {!showBooks?   this.showSearchFields() : ""}
      
      { showBooks ?    
   <div className="row">
   <div className="col-3">
    <LeftPannel   onOptionChange={this.onOptionChangeHandler} option={queryParams}/>
       </div>
       <div className="col-9">

<BooksNav  handlePage={this.handlePage} handleMyBooks={this.addToMyBooksHandler} option={queryParams}
              Books={Books}/>
       </div>
       </div>
        
       : "" }
      </React.Fragment>);
    }
}
export default BooksBySearch;