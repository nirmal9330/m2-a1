import React,{Component} from "react";

class LeftPannel extends Component
{
    state={
        language:[{display:"English",value:"en"},
        {display:"French",value:"fr"},
        {display:"Hindi",value:"hi"},
        {display:"Spanish",value:"es"},
        {display:"Chinese",value:"zh"}],
        filters:[{display:"Full Volume",value:"full"},
        {display:"Partial Volume",value:"partial"},
        {display:"Free Google e-Books",value:"free-ebooks"},
        {display:"Paid Google e-Books",value:"paid-ebooks"},],
        printTypes:[{display:"All",value:"all"},
        {display:"Books",value:"books"},
        {display:"Magazines",value:"magazines"}],
        OrderBy:[{display:"relevance ",value:"relevance"},
        {display:"newest",value:"newest"}],
        OrderBy:["relevance","newest"],
        option:require("./settingOption.js"),
    };



    showRadio=(label,arr,name,Selvalue)=>
    {
        return(<React.Fragment>
            <label className="form-check-label font-weight-bold text-dark">{label}</label>
            <hr className="border-dark"/>
           
            {arr.map((el,index)=>(
              
                  <div className="form-check border-primary bg-light" key={index}>

                <input className="from-check-input " type="radio" name={name} value={el.value}
                onChange={this.handleChange}  checked={el.value===Selvalue}></input>
                <label className="from-check-label">{el.display}</label>
               
                </div>
              
            ))}
           
        </React.Fragment>)
    }

    
showDropDown=(label,arr,name,selVal)=>
{
        return(<React.Fragment>
           <div className="form-group">
                       <label>{label}</label>
                       <select className="form-control" id={name}
                       name={name} value={selVal} onChange={this.handleChange} >
                           <option disabled value="">
                              {label}
                          </option>
                          
                { arr.map((c,index)=>( <option key={index}>{c}</option>))}

                       </select>
                   </div>
           
        </React.Fragment>);
};


handleChange=(e)=>
{  
    let {currentTarget : input}=e;
    let optionx=this.props.optionx;
    optionx[input.name]=input.value;
    console.log(input.value);
    this.props.onOptionChange(optionx);
}

    render()
    {
        console.log("Led");
        console.log(this.props);
        let {option}=this.state;
        let {language,filters,printTypes,OrderBy}=this.state;
        let {langRestrict="",filter="",printType="",orderBy=""}=this.props.optionx;
         return (<React.Fragment>
               <div className="container border border-light bg-light " style={{marginTop:'18%', height:'100%' }}>
         <h5 className="text-center"></h5>
       {option.option.language===true ?(<React.Fragment>
       <div className="conatiner  border border-dark font-weight" style={{marginTop:'5%'}}>
     {this.showRadio("Language",language,"langRestrict",langRestrict)}
     </div><br/>
     </React.Fragment>) :""}
      {option.option.filter===true ?(<React.Fragment>
        <div className="conatiner  border border-dark font-weight">
              {this.showRadio("Filter",filters,"filter",filter)}
              </div><br/>
              </React.Fragment>) :""}
              {option.option.printType===true ?(<React.Fragment>
              <div className="conatiner  border border-dark font-weight">
              {this.showRadio("PrintType",printTypes,"printType",printType)}
              </div><br/><br/><br/><br/>
              </React.Fragment>) :""}
              {option.option.orderBy===true ?(<React.Fragment>
              <div className="conatiner  border border-dark font-weight">
              {this.showDropDown("OrderBy",OrderBy,"orderBy",orderBy)}
            </div>
            </React.Fragment>) :""}
            </div>
         </React.Fragment>)
    }
}
export default LeftPannel;