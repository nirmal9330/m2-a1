import React,{Component} from "react"
import queryString from "query-string";
import LeftPannel from "./leftPannelBooks";
import BooksNav from "./booksDisplay";
import http from "../Servecies/serverBook";
import {Route,Redirect,Switch} from "react-router-dom";
class Books extends Component
{
state={
    BooksArr:[],
    option:require("./settingOption.js"),
    
};

async componentDidMount()
{

    console.log("Mounted");
    let queryParams=queryString.parse(this.props.location.search);
    let searchString=this.makeSearchstring(queryParams);
    this.props.history.push({pathname:"/books",search:searchString});
    this.fetchData();
}

async componentDidUpdate(prevProps,prevState)
{
  console.log("Updated Component");
  console.log("PP",prevProps);
  console.log("TP",this.props);
  console.log("PS",prevState);
  console.log("TS",this.state);
  if(prevProps.location.search!==this.props.location.search)
  {
      this.fetchData();
  }
}

async fetchData()
{
  
    let queryParams=queryString.parse(this.props.location.search);
    let searchString=this.makeSearchstring(queryParams);
    console.log("str",searchString);
    let response= await http.get(`&${searchString}`);
    let {data}=response;
    console.log("data",data);
    this.setState({BooksArr:data.items});
    console.log("STR",searchString);
   this.props.history.push({pathname:"/books",search:searchString});
    console.log("DF");
}


makeSearchstring=(queryParams)=>
{
    let {value}=this.props;
    let {option}=require("./settingOption.js");
      console.log("SIT",option.startIndex);
    let {maxResults=option.maxSize,langRestrict,orderBy,printType,filter,q=value,startIndex="0"}=queryParams;
     //let ;
    let searchString="";

    searchString=this.addToQueryString(searchString,"q",q);
    searchString=this.addToQueryString(searchString,"printType",printType);
    searchString=this.addToQueryString(searchString,"langRestrict",langRestrict);
    searchString=this.addToQueryString(searchString,"filter",filter);
    searchString=this.addToQueryString(searchString,"orderBy",orderBy);
    
    searchString=this.addToQueryString(searchString,"startIndex",startIndex);
    searchString=this.addToQueryString(searchString,"maxResults",maxResults);
  
    return searchString;



}

addToQueryString=(str,name,value)=>
value ? str ? `${str}&${name}=${value}`:
 `${name}=${value}` : str ;


 handlePage=(index)=>
 {
 let {option}=require("./settingOption.js");
 let queryParams=queryString.parse(this.props.location.search);
 queryParams.startIndex=(+queryParams.startIndex)+(index*(queryParams.maxResults));
 console.log("SI",queryParams.startIndex);
 //option.startIndex=option.startIndex+(index*(option.maxSize));
 //this.fetchData();
 this.callURL("/books",queryParams);
 }


 
 addToMyBooksHandler=(obj,value)=>
 {
    let {MyBookCart}=require("./cart1.js");
    let s1={...this.state};
    if(value==="Add")
    {
      
        MyBookCart.push(obj);
    }
    else if(value==="Remove")
    {
        let index1=MyBookCart.findIndex((el)=>el.id===obj.id);
       
        MyBookCart.splice(index1,1);
    }
  this.setState(s1);
 }

 
 callURL=(url,option)=>
 {

 let searchString=this.makeSearchstring(option);
 this.props.history.push({
   pathname:url,
   search:searchString,
});
}

 

 onOptionChangeHandler=(option)=>
 {
    this.callURL("/books",option);
 }
 render()
 {
    
     let {BooksArr}=this.state;
     let queryParams=queryString.parse(this.props.location.search);
   
     return(<React.Fragment>
         <div className="container">
          <div className="row">
              <div className="col-3">
              <LeftPannel  optionx={queryParams} onOptionChange={this.onOptionChangeHandler}/>
           </div>
             <div className="col-9">
              <BooksNav  handlePage={this.handlePage} handleMyBooks={this.addToMyBooksHandler} option={queryParams}
              Books={BooksArr}/>
            
               </div>
              </div>
        </div>
            
     </React.Fragment>);
 }
}
export default Books;