import React,{Component} from "react";
import http from "../Servecies/serverBook";
import LeftPannel from "./leftPannelBooks";
import queryString from "query-string";
class BooksNav extends Component
{
    state={
        MyBookCart:require("./cart1.js"),
    };


    handlePage=(index)=>
    {
       this.props.handlePage(index);
    }
    addToMyBooksHandler=(obj,str)=>
    {
          this.props.handleMyBooks(obj,str);
    }
    render()
    {
      let {Books}=this.props;
      console.log("BK ",Books);
      let {q="",startIndex="",maxResults=""}=this.props.option;
      let {MyBookCart}=this.state;
        return (<React.Fragment>
            <div className="container">
            <div className="col-12 text-center">
                     <h4 className="text-info">{q} Books</h4>
               
                 <div className="col-3 ">
                     <h5 className="text-success">{startIndex}-{+maxResults+(+startIndex)} Entries</h5>
                 </div>
                <div className="col-12" style={{marginTop:'0%', height:'100%' }}>
                  <div className="row text-center">
                   { Books!==undefined ?Books.map((ele,index)=>(
                       <div className="col-3 border  bg-success" key={index}>
                   <React.Fragment>
                  <div className="text-center">
                   <img src={ele.volumeInfo.imageLinks?ele.volumeInfo.imageLinks.thumbnail
                     :""}></img>
                     </div>
                     <div className="text-center">
                      <h4>{ele.volumeInfo.title}</h4>
                       </div>
                      <div className="text-center">
                       {ele.volumeInfo.authors?ele.volumeInfo.authors.map((el)=>(<p>{el}</p>)): ""}
                        </div>
                         <div className="text-center">
                         {  ele.volumeInfo.categories ? ele.volumeInfo.categories.map((el)=>(<p>{el}</p>)) : ""}
                        </div>
                         <div className="text-center align-bottom margin-bottom-auto"  style={ {alignContent:'bottom',
                         marginBottom:'2%',alignItems:'bottom',justifyContent:'center' ,float:'end',marginBottom:'auto'}} >
                        <button className="btn btn-secondary"
                    onClick={()=>this.addToMyBooksHandler(ele,MyBookCart.MyBookCart.find((el)=>el.id===ele.id  )?"Remove":"Add")}>
                      {MyBookCart.MyBookCart.find((el)=>el.id===ele.id)?"Remove from My Book" : "Add To My Books"}</button>
                      </div>
                      </React.Fragment>
                     </div>
                  )) : <h3 className="bg-warning"> No Books find</h3>}
                </div>
                <br/><br/>
               <div className="row">
                 {+startIndex>0 ?
                 <div className="col-2">
                     <button className="btn btn-warning"  onClick={()=>this.handlePage(-1)}>Prev</button>
                 </div>: ""}
                 <div className="col-8"></div>
                 {+maxResults<=1000 ?
                 <div className="col-2"> <button className="btn btn-warning"
                 onClick={()=>this.handlePage(+1)}>Next</button></div>
                 :""}
                 </div>
               </div>
               </div>
               </div>
              
              
        </React.Fragment>)
    }
}
export default BooksNav;